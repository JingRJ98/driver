type allowedEvents =
  | "overlayClick"
  | "nextClick"
  | "prevClick"
  | "closeClick"
  | "escapePress" // esc按键回调
  | "arrowRightPress" // 右箭头按键回调
  | "arrowLeftPress" // 左箭头按键回调

let registeredListeners: Partial<{ [key in allowedEvents]: () => void }> = {};

export function listen(hook: allowedEvents, callback: () => void) {
  registeredListeners[hook] = callback;
}

export function emit(hook: allowedEvents) {
  registeredListeners[hook]?.();
}

export function destroyEmitter() {
  registeredListeners = {};
}
