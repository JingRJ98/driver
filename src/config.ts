import { DriveStep } from "./driver";
import { AllowedButtons, PopoverDOM } from "./popover";
import { State } from "./state";

export type DriverHook = (element: Element | undefined, step: DriveStep, opts: { config: Config; state: State }) => void;

export type Config = {
  steps?: DriveStep[];
  /** 是否为产品导览设置动画。（默认值：true） */
  animate?: boolean;
  /** 背景颜色 (default: black)*/
  overlayColor?: string;
  /** (default: 0.5) */
  overlayOpacity?: number;
  /** 是否平滑滚动到突出显示的元素。（默认值：false） */
  smoothScroll?: boolean;
  /** (default: true) */
  allowClose?: boolean;
  /** 突出显示的元素和视口之间的padding。（默认值：10px） */
  stagePadding?: number;
  /** 视口的radius (default: 5) */
  stageRadius?: number;
  /** 是否禁用与高亮显示的元素之间的交互 (default: false) */
  disableActiveInteraction?: boolean;
  /** 是否允许使用键盘控制 */
  allowKeyboardControl?: boolean;

  // Tooltiip弹框的配置
  popoverClass?: string;
  popoverOffset?: number;
  showButtons?: AllowedButtons[];
  disableButtons?: AllowedButtons[];
  showProgress?: boolean;

  // Button texts
  progressText?: string;
  nextBtnText?: string;
  prevBtnText?: string;
  doneBtnText?: string;

  // Called after the popover is rendered.
  // PopoverDOM is an object with references to the popover DOM elements such as buttons title, descriptions, body, container etc.
  onPopoverRender?: (popover: PopoverDOM, opts: { config: Config; state: State }) => void;

  // Hooks to run before and after highlighting
  // each step. Each hook receives the following parameters:
  //   - element: The target DOM element of the step
  //   - step: The step object configured for the step
  //   - options.config: The current configuration options
  //   - options.state: The current state of the driver
  onHighlightStarted?: DriverHook;
  onHighlighted?: DriverHook;
  onDeselected?: DriverHook;
  onDestroyStarted?: DriverHook;
  onDestroyed?: DriverHook;
};

let currentConfig: Config = {};

export function setConfig(config: Config = {}) {
  currentConfig = {
    animate: true,
    allowClose: true,
    overlayOpacity: 0.7,
    smoothScroll: false,
    disableActiveInteraction: false,
    showProgress: false,
    stagePadding: 10,
    stageRadius: 5,
    popoverOffset: 10,
    showButtons: ["next", "previous", "close"],
    disableButtons: [],
    overlayColor: "#000",
    ...config,
  };
}

export function getConfig(): Config;
export function getConfig<K extends keyof Config>(key: K): Config[K];
export function getConfig<K extends keyof Config>(key?: K) {
  return key ? currentConfig[key] : currentConfig;
}
