import { StageDefinition } from "./overlay";
import { PopoverDOM } from "./popover";
import { DriveStep } from "./driver";

export type State = {
  /** 是否已经初始化过 */
  isInitialized?: boolean;

  /** 当前到第几步 */
  activeIndex?: number;
  /** 当前选中的Dom */
  activeElement?: Element;
  /** 当前步的相关对象 */
  activeStep?: DriveStep;
  /** 前一步选中的Dom */
  previousElement?: Element;
  /** 前一步的相关对象 */
  previousStep?: DriveStep;

  /** 弹出窗口的DOM元素，包括容器、标题、描述、按钮等。 */
  popover?: PopoverDOM;

  //考虑动画的实际值 和延迟。这些是用来求出位置等内部使用的state
  __previousElement?: Element;
  __activeElement?: Element;
  __previousStep?: DriveStep;
  __activeStep?: DriveStep;

  __activeOnDestroyed?: Element;
  __resizeTimeout?: number;
  __transitionCallback?: () => void;
  __activeStagePosition?: StageDefinition;
  __overlaySvg?: SVGSVGElement;
};

let currentState: State = {};

export const setState = <K extends keyof State>(key: K, value: State[K]) => {
  currentState[key] = value;
}

export function getState(): State;
export function getState<K extends keyof State>(key: K): State[K];
export function getState<K extends keyof State>(key?: K) {
  return key ? currentState[key] : currentState;
}

export function resetState() {
  currentState = {};
}
